import pylast
import urllib3
import json

class Ratio(object):
    def __init__(self):
        self.LASTFM_KEY = ""
        self.LASTFM_SECRET = ""
        
        self.lastfm_user = ""
        self.lastfm_pass = ""
        self.lastfm_pass_hash = pylast.md5(self.lastfm_pass)

    def __getscrobbles(self, song_title, artist_title):
        playcount = ""

        network = pylast.LastFMNetwork(api_key = self.LASTFM_KEY,
                api_secret = self.LASTFM_SECRET,
                username = self.lastfm_user,
                password_hash = self.lastfm_pass_hash)
        if song_title != "":
            try:
                track = network.get_track(artist_title, song_title)
                playcount = track.get_playcount()
            except (Exception, pylast.WSError) as error:
                print ("Error fetching song info! => ", error)


        return playcount

    def __getwmtudata(self):
        http = urllib3.PoolManager()
        
        try:
            r = http.request('GET', 'http://log.wmtu.fm/api/2.0/stats?n=1000')
        except urllib3.exceptions.ConnectionError:
            print("something went wrong, I will add error handling later")

        if r.status != 200:
            print("something went wrong, I will add error handling later")

        data = r.data
        decoded_data = data.decode('utf-8')
        request_return = json.loads(decoded_data)

        return request_return

    def getdata(self):
        wmtu_plays = 1
        lastfm_plays = 1
        wmtu_data = self.__getwmtudata()

        for track in range (1, len(wmtu_data)):
            song_title = wmtu_data[track]['song']
            artist_title = wmtu_data[track]['artist']
            wmtu_plays = wmtu_data[track]['play_count']

            lastfm_plays = self.__getscrobbles(song_title, artist_title)

            ratio = (wmtu_plays/lastfm_plays)*100

            if ( ratio > 1 ):
                print (f'{song_title} - {artist_title}: {ratio}')



def main():
    ratio = Ratio()

    ratio.getdata()

if __name__ == '__main__':
    try:
        main()
    except Exception as err:
        print("something went wrong, I will add error handling later")




